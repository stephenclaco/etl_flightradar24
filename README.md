**Projet ETL - Extraction, Transformation et Chargement de données avec PySpark**


Ce projet vise à effectuer le processus ETL (Extraction, Transformation et Chargement) en utilisant PySpark pour analyser et traiter des données de vols provenant de FlightRadar24 API. Le code fourni extrait les informations des compagnies aériennes, des aéroports, des vols et des zones, puis effectue des transformations sur les données extraites.

**Prérequis**
- Python 3.x
- PySpark
- FlightRadarAPI

**Installation**

1. Assurez-vous d'avoir Python 3.x installé sur votre système.
2. Insatllez PySpark en exécutant la commande suivante :
`pip install pyspark`
3. Installez FlightRadarAPI en exécutant la commande suivante :
`pip install FlightRadarAPI`

**Configuration**


La configuration du projet se trouve dans le fichier config.py. Vous pouvez modifier les paramètres de configuration tels que les clés d'API, les chemins de fichiers de sortie, etc.`

**Exécution**

Pour exécuter le projet, suivez les étapes suivantes :

1. Exécutez le script etl.py pour effectuer le processus ETL complet.

`python etl.py`

2. Les données extraites seront stockées dans les fichiers de sortie spécifiés dans la configuration.

**Structure du code**


- etl.py: Fichier principal contenant les fonctions d'extraction, de transformation et de chargement des données.
- config.py: Fichier de configuration contenant les paramètres de configuration du projet.
- etl_dag.py : Fichier clé du pipeline ETL de ce projet. Il définit le flux de travail du DAG, garantit des exécutions régulières, la gestion des erreurs et la traçabilité des opérations. Ce fichier assure la qualité et la cohérence des données tout au long du processus ETL, avec des mises à jour régulières toutes les 2 heures.

**Fonctions principales**

1. extract_flight_information(): Fonction d'extraction des données des compagnies aériennes, des aéroports, des vols et des zones à partir de FlightRadar24 API.
2. transform_clean_data(): Fonction de transformation des données extraites, y compris la mise à jour des noms de pays et de continents, et la conversion des types de données.
3. load_data(): Fonction de chargement des données transformées dans des fichiers de sortie.
4. etl_Flight() : Fonction qui execute tout les précedentes Fonctions
